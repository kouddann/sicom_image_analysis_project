import numpy as np
from scipy.signal import convolve2d

from src.forward_model import CFA

def naive_interpolation(op: CFA, y: np.ndarray) -> np.ndarray:
    
    z = op.adjoint(y)

    if op.cfa == 'bayer':
        res = np.empty(op.input_shape)
        
        ker_bayer_red_blue = np.array([[1, 2, 1], [2, 4, 2], [1, 2, 1]]) / 4

        ker_bayer_green = np.array([[0, 1, 0], [1, 4, 1], [0, 1, 0]]) / 4

        res[:, :, 0] = convolve2d(z[:, :, 0], ker_bayer_red_blue, mode='same')
        res[:, :, 1] = convolve2d(z[:, :, 1], ker_bayer_green, mode='same')
        res[:, :, 2] = convolve2d(z[:, :, 2], ker_bayer_red_blue, mode='same')

    else:
        res = np.empty(op.input_shape)

        res[:, :, 0] = varying_kernel_convolution(z[:, :, 0], K_list_red)
        res[:, :, 1] = varying_kernel_convolution(z[:, :, 1], K_list_green)
        res[:, :, 2] = varying_kernel_convolution(z[:, :, 2], K_list_blue)

    return res

